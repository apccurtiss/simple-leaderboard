from datetime import datetime
import logging
import sys
from typing import Any, Dict, Tuple, Union

import click
from flask import Blueprint, Flask, g, redirect, render_template, request, session, url_for
from flask_wtf import FlaskForm # type: ignore
from werkzeug.wrappers import Response
from wtforms import StringField # type: ignore
from wtforms.validators import DataRequired, Length # type: ignore

from lib.secrets import get_secret # type: ignore
from lib.database import Database, User

View = Union[Response, str, Tuple[str, int]]

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
for logger_name in [
    'werkzeug',
    'engineio.server'
]:
    logging.getLogger(logger_name).disabled = True

app = Flask(__name__)
app.config['SECRET_KEY'] = get_secret('app_secret_key.secret')
app.config['ADMIN_TOKEN'] = get_secret('admin_token.secret')
app.jinja_env.globals.update(zip=zip)

core = Blueprint('core', __name__)

DB = Database()

@app.before_request
def before_request():
    g.session = DB.get_session()
    if 'id' not in session:
        user = g.session.create_user()
        session['id'] = user.id

@app.after_request
def after_request(response):
    g.session.close()

    return response

@app.context_processor
def inject_username() -> Dict[str, Any]:
    user = g.session.get_user(session['id'])
    if not user:
        user = g.session.create_user()
        session['id'] = user.id

    return {
        'leaderboards': g.session.get_leaderboards(),
        'username': user.username,
    }

@core.route('/')
def index() -> View:
    users = g.session.get_users()
    return render_template('index.html')

@core.route('/faq')
def faq() -> View:
    return render_template('faq.html')

@core.route('/leaderboard/<leaderboard_id>')
def leaderboard(leaderboard_id: int) -> View:
    leaderboard = g.session.get_leaderboard(leaderboard_id)
    completion_times = g.session.get_completion_times(leaderboard_id)

    return render_template('leaderboard.html', leaderboard=leaderboard, completion_times=completion_times)

@core.route('/submit/<token>')
def submit(token: str) -> View:
    try:
        leaderboard = g.session.complete_challenge(session['id'], token)
    except:
        return redirect(url_for('core.index'))

    return redirect(url_for('core.leaderboard', leaderboard_id=leaderboard.id))

class RenameForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=1, max=128)])

@core.route('/rename', methods=['GET', 'POST'])
def rename() -> View:
    form = RenameForm()

    if form.validate_on_submit():
        if form.username.data == 'admin':
            form.username.errors.append('Sorry, the admin user is reserved!')
        elif form.username.data == 'admin-' + app.config['ADMIN_TOKEN']:
            g.session.rename_user(session['id'], 'admin')
            session['username'] = 'admin'
            return redirect(url_for('core.index'))
        else:
            g.session.rename_user(session['id'], form.username.data)
            session['username'] = form.username.data
            return redirect(url_for('core.index'))

    return render_template('rename.html', form=form)

@core.route('/admin')
def admin() -> View:
    if session.get('username') != 'admin':
        return redirect(url_for('core.index'))

    leaderboards = g.session.get_leaderboards()
    return render_template('admin.html', leaderboards=leaderboards)

@core.route('/admin/new', methods=['POST'])
def new_leaderboard() -> View:
    if session.get('username') != 'admin':
        return redirect(url_for('core.index'))

    g.session.new_leaderboard()
    return redirect(url_for('core.admin'))

@core.route('/admin/update', methods=['POST'])
def update_leaderboard() -> View:
    if session.get('username') != 'admin':
        logger.info(f"Failed attempt to update from {session.get('username')}")
        return redirect(url_for('core.index'))

    if request.form.get("action") == "Delete":
        g.session.delete_leaderboard(request.form["id"])
    else:
        g.session.update_leaderboard(
            request.form["id"],
            request.form["name"],
            request.form["token"],
            request.form["description"],
            "visible" in request.form,
        )

    return redirect(url_for('core.admin'))

@click.command()
@click.option('--debug', is_flag=True)
@click.option('--port', default=80)
def main(debug: bool, port: int) -> None:
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)

    logger.info('Starting up...')
    logger.info('Access admin with token: admin-' + app.config["ADMIN_TOKEN"])
    
    app.register_blueprint(core)
    app.run('0.0.0.0', debug=debug, port=port)

if __name__ == '__main__':
    main()