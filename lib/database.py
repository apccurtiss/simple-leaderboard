from datetime import datetime
from random import randint
import random
import string
from typing import List, Optional

import pytz # type: ignore
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, Session
from sqlalchemy import create_engine, join
 
Base = declarative_base()


# from lib.secrets import get_secret

class Completion(Base): # type: ignore
    __tablename__ = 'completions'

    user_id = Column(String(256), ForeignKey('users.id'), primary_key=True)
    leaderboard_id = Column(String(256), ForeignKey('leaderboards.id'), primary_key=True)
    time = Column(DateTime)

class User(Base): # type: ignore
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(256))
    score = Column(Integer)
    completions: relationship = relationship(Completion)

class Leaderboard(Base): # type: ignore
    __tablename__ = 'leaderboards'

    id = Column(Integer, primary_key=True)

    name = Column(String(256))
    token = Column(String(256))
    description = Column(String(256))
    visible = Column(Boolean)
    completions: relationship = relationship(Completion)

class DBSession:
    def __init__(self, session: Session):
        self.session = session

    def close(self) -> None:
        self.session.close()

    def create_user(self) -> User:
        user = User(username='Guest #{}'.format(randint(0, 1000)), score=0)
        self.session.add(user)
        self.session.commit()

        return user

    def get_user(self, id: str) -> Optional[User]:
        return self.session.query(User).filter_by(id=id).first()

    def rename_user(self, id: str, username: str):
        self.session.query(User).filter_by(id=id).update({'username': username})
        self.session.commit()

    def complete_challenge(self, user_id: str, token: str) -> Leaderboard:
        leaderboard = self.session.query(Leaderboard).filter(Leaderboard.token==token).first()
        if not leaderboard:
            raise ValueError()

        completion = Completion(time=datetime.now(pytz.timezone('US/Mountain')), user_id=user_id, leaderboard_id=leaderboard.id)

        self.session.merge(completion)
        self.session.commit()

        return leaderboard

    def get_users(self) -> List[User]:
        return list(self.session.query(User))

    def get_leaderboards(self) -> List[Leaderboard]:
        return list(self.session.query(Leaderboard).all())

    def get_leaderboard(self, leaderboard_id: str) -> Optional[Leaderboard]:
        query = self.session.query(Leaderboard).filter(Leaderboard.id==leaderboard_id)
        return query.first()
    
    def get_completion_times(self, leaderboard_id: str) -> List[Completion]:
        query = (self.session.query(User, Completion.time) # type: ignore
            .outerjoin(Completion, User.id==Completion.user_id)
            .filter(Completion.leaderboard_id==leaderboard_id))
        return query.all()
    
    def new_leaderboard(self) -> None:
        self.session.add(Leaderboard(
            name='No name',
            description='No description',
            token=''.join(random.choice(string.ascii_letters) for i in range(10)),
            visible=False,
        ))
        self.session.commit()

    def delete_leaderboard(self, leaderboard_id) -> None:
        self.session.query(Leaderboard).filter(Leaderboard.id == leaderboard_id).delete()
        self.session.commit()

    def update_leaderboard(self, leaderboard_id: str, name: str, token: str, description: str, visible: bool) -> None:
        self.session.query(Leaderboard).filter(Leaderboard.id == leaderboard_id).update({
            Leaderboard.name: name,
            Leaderboard.token: token,
            Leaderboard.description: description,
            Leaderboard.visible: visible,
        })
        self.session.commit()

class Database:
    def __init__(self):
        engine = create_engine('sqlite:///data/leaderboards.db')
        Base.metadata.create_all(engine)
        self.sessionmaker = sessionmaker(bind=engine)
    
    def get_session(self) -> DBSession:
        return DBSession(self.sessionmaker())