from pathlib import Path

def get_secret(file: str) -> str:
    return Path('secrets', file).read_text()